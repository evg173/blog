class UsersController < ApplicationController
    load_and_authorize_resource

    def show
        options = { include: [:posts] }
        render json: UserSerializer.new(@user, options).serializable_hash
    end

    def update
        if @user.update(user_params)
            render json: UserSerializer.new(@user).serializable_hash, status: :created            
        else
            render json: @user.errors, status: :unprocessable_entity
        end
    end

    def destroy
        @user.destroy
        head 204
    end

    private

    def user_params
        params.require(:user).permit(:email, :password)
    end

    def set_user
        @user = User.find(params[:id])
    end
end