class PostsController < ApplicationController
    include Paginable   

    load_and_authorize_resource

    def index
        @posts = Post.page(current_page)
                    .per(per_page)
        
        options = {
            links: {
                first: posts_path(page: 1),
                last: posts_path(page: @posts.total_pages),
                prev: posts_path(page: @posts.prev_page),
                next: posts_path(page: @posts.next_page),
            }
        }

        options[:include] = [:user]
        render json: PostSerializer.new(@posts, options).serializable_hash
    end

    def show
        options = { include: [:user] }
        render json: PostSerializer.new(@post, options).serializable_hash
    end

    def create
        post = current_user.posts.build(post_params)
        post.user_id = current_user.id
        if post.save
            render json: PostSerializer.new(post).serializable_hash, status: :created
        else
            render json: { errors: post.errors }, status: :unprocessable_entity
        end
    end

    def update
        if @post.update(post_params)
            render json: PostSerializer.new(@post).serializable_hash
        else
            render json: @post.errors, status: :unprocessable_entity
        end
    end

    def destroy
        @post.destroy
        head 204
    end
      
    private

    def post_params
        params.require(:post).permit(:title, :body)
    end
       
    def set_post
        @post = Post.find(params[:id])
    end
end