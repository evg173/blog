class UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes :email, :admin, :moderator
  has_many :posts
end
