class Post < ApplicationRecord
    validates :title, :user_id, presence: true
    validates :body, presence: true

    belongs_to :user
end
