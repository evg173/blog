class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise  :database_authenticatable, 
          :jwt_authenticatable,
          :registerable,
          jwt_revocation_strategy: JwtDenylist


  validates :email, uniqueness: true
  validates_format_of :email, with: /@/
  validates :encrypted_password, presence: true

  has_many :posts
end
